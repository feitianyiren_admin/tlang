# tlang

tlang是用来去除区块链的虚拟机的类lisp语言

因为区块链的虚拟机不是必要的，改以解释执行tlang的形式表达智能合约，然后提供标准交易模板，普通交易使用交易模板进行，有复杂逻辑的交易由应用开发者使用tlang语言表达即可（也可以使用面向java、.net、js、go等语言的sdk书写，使用sdk书写的程序会自动翻译为tlang）。

# 案例
 **下面举例用tlang书写的一段合约逻辑** 
> 这个合约的逻辑是，如果想要该合约的执行结果为true：要么知道我的birthDay是2017-06-23 要么 给出pubKey和用对应的私钥对该交易的签名。
>  
> 为了使合约执行结果为true，调用者需提供正确的birthDay pubKey sign

 **合约调用方提供的数据：** 

```
(define birthDay "{birthDay}")
(define pubKey "{pubKey}")
(define sign "{sign}")
```

> 上面的键值对来自合约调用者，数据是键值对，执行时直接把键值对加入作用域不需要拼代码从而没有注入代码的可能。
> 
> 下面三行是智能合约的逻辑，这三行是死的不会被调用者注入代码。

 **合约定义方定义的逻辑：** 

```
(define pubKeyHash="8658cefd0c1cf0ff294d14a66278f5a5490a1f8e")
(define myBirthDayHash="d4a0f6c5b4bcbf2f5830eabed3daa7304fb794d6")
(or 
	(= (hash160 birthDay) myBirthDayHash) 
	(and 
		(= (hash160 pubKey) pubKeyHash) 
		(= (decrypt pubKey sign) txHash)))
```

> and = hash160 or decrypt是系统函数，txHash或者叫txId是当前交易记录的hash。
> 
> 如果提供checksig系统函数的话(= (decrypt pubKey sign) txHash)也可以是(checksig pubKey sign txHash)。

> ps:话说，这个案例不好，因为生日可以被枚举出来，从今天往前推100年挨着计算每一天的hash然后跟myBirthDayHash比对的话就能取走被这个合约锁定的资产了。
<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=512e35f77596c1ecab560b28165d28d19af7ba7c014e489cac0e55752cfb08a0"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt=".NET区块链" title=".NET区块链"></a>