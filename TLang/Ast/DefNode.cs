﻿using System;

namespace TLang.Ast
{
    using Values;

    public class DefNode : Node
    {
        private readonly Node _pattern;
        private readonly Node _type;
        private readonly Node _value;


        public DefNode(Node pattern, Node type, Node value, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this._pattern = pattern;
            this._type = type;
            this._value = value;
        }


        public override Value Interp(Scope s)
        {
            Value value = _value.Interp(s);
            Binder.CheckDup(_pattern, s);
            Value valueType = _type?.Interp(s);
            Binder.Define(_pattern, (valueType as TType), value, s);
            return value;
        }

        public override String ToString()
        {
            string type = _type != null ? " " + _type.ToString() : string.Empty;
            return "(" + Constants.DefKeyword + " " + _pattern + type + " " + _value + ")";
        }
    }
}
