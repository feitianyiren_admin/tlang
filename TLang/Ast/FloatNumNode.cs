﻿using System;

namespace TLang.Ast
{
    using Values;

    public class FloatNumNode : Node
    {
        public readonly String Content;
        private readonly double _value;


        public FloatNumNode(String content, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Content = content;
            this._value = Double.Parse(content);

        }


        public static FloatNumNode Parse(String content, String file, int start, int end, int line, int col)
        {
            try
            {
                return new FloatNumNode(content, file, start, end, line, col);
            }
            catch (FormatException)
            {
                return null;
            }
        }


        public override Value Interp(Scope s)
        {
            return new FloatValue(_value);
        }

        public override String ToString()
        {
            return _value.ToString();
        }
    }
}
