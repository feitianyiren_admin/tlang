﻿using System;
namespace TLang.Ast
{
    using Values;

    public class KeywordNode : Node
    {
        public readonly String Id;


        public KeywordNode(String id, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Id = id;
        }


        public NameNode AsName()
        {
            return new NameNode(Id, File, Start, End, Line, Col);
        }


        public override Value Interp(Scope s)
        {
            var value = s.Lookup(Id);
            if (value == null)
            {
                throw Util.GeneralError(this.ToString() + " is'not a keyword");
            }
            return value;
        }

        public override String ToString()
        {
            return Constants.ColonChar + Id;
        }
    }
}
