﻿using System;
using System.Collections.Generic;
using TLang.Values;
using System.Linq;

namespace TLang.Ast
{
    public class AndNode : Node
    {
        public readonly List<Node> Elements;

        public AndNode(List<Node> elements, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Elements = elements;
        }

        public override Value Interp(Scope s)
        {
            for (int i = 0; i < this.Elements.Count; i++)
            {
                if (!InterpNode(this.Elements[i], s))
                {
                    return BoolValue.False;
                }
            }

            return BoolValue.True;
        }

        private static bool InterpNode(Node node, Scope s)
        {
            Value value = node.Interp(s);
            if (!(value is BoolValue boolValue))
            {
                throw Util.GeneralError(value.ToString() + " is not BoolValue type");
            }
            return boolValue.Value;
        }

        public override string ToString()
        {
            return $"({Constants.AndKeyword} {string.Join(" ", Elements?.Select(a => a.ToString()))})";
        }
    }
}
