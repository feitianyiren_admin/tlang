﻿using System;
using System.Collections.Generic;
using System.Linq;
using TLang.Values;

namespace TLang.Ast
{
    public class ForNode : Node
    {
        private readonly List<Node> _elements;


        public ForNode(List<Node> elements, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this._elements = elements;
        }

        public override Value Interp(Scope s)
        {
            Scope scope = new Scope(s);
            this._elements[0].Interp(scope);
            Value result = VoidValue.Void;
            while (true)
            {
                Value test = this._elements[1].Interp(scope);
                if (test is BoolValue b)
                {
                    if (b.Value)
                    {
                        for (int i = 3; i < _elements.Count; i++)
                        {
                            if (i == _elements.Count - 1)
                            {
                                result = this._elements[i].Interp(scope);
                            }
                            else
                            {
                                this._elements[i].Interp(scope);
                            }
                        }
                        this._elements[2].Interp(scope);
                    }
                    else
                    {
                        return result;
                    }
                }
                else
                {
                    throw Util.GeneralError("test result is not BoolValue type");
                }
            }
        }

        public override string ToString()
        {
            return $"({Constants.ForKeyword} {string.Join(" ", this._elements.Select(a => a.ToString()))})"; ;
        }
    }
}
