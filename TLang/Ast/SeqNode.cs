﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TLang.Ast
{
    using Values;

    public class SeqNode : Node
    {
        public readonly List<Node> Statements;


        public SeqNode(List<Node> statements, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Statements = statements ?? new List<Node>();
        }


        public override Value Interp(Scope s)
        {
            s = new Scope(s);
            for (int i = 0; i < Statements.Count - 1; i++)
            {
                Statements[i].Interp(s);
            }
            return Statements[Statements.Count - 1].Interp(s);
        }

        public override String ToString()
        {
            StringBuilder sb = new StringBuilder();
            String sep = Statements.Count > 5 ? "\n" : " ";
            sb.Append("(").Append(Constants.SeqKeyword).Append(sep);

            for (int i = 0; i < Statements.Count; i++)
            {
                sb.Append(Statements[i].ToString());
                if (i != Statements.Count - 1)
                {
                    sb.Append(sep);
                }
            }

            sb.Append(")");
            return sb.ToString();
        }
    }
}
