﻿using System;
using System.Collections.Generic;
using System.Linq;
using TLang.Values;

namespace TLang.Ast
{
    public class OrNode : Node
    {
        public readonly List<Node> Elements;

        public OrNode(List<Node> elements, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Elements = elements;
        }

        public override Value Interp(Scope s)
        {
            for (int i = 0; i < this.Elements.Count; i++)
            {
                if (InterpNode(this.Elements[i], s))
                {
                    return BoolValue.True;
                }
            }

            return BoolValue.False;
        }

        private static bool InterpNode(Node node, Scope s)
        {
            Value value = node.Interp(s);
            if (!(value is BoolValue boolValue))
            {
                throw Util.GeneralError(value.ToString() + " is not BoolValue type");
            }
            return boolValue.Value;
        }

        public override string ToString()
        {
            return $"({Constants.OrKeyword} {string.Join(" ", Elements?.Select(a => a.ToString()))})";
        }
    }
}
