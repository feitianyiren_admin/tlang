﻿using System;
using System.Linq;

namespace TLang.Ast
{
    using System.Collections.Generic;
    using Values;

    public class DelimeterNode : Node
    {
        #region static members

        // all delimeters
        private static readonly HashSet<String> Delims = new HashSet<String> {
            Constants.ParenBegin, Constants.ParenEnd,
            Constants.CurlyBegin, Constants.CurlyEnd,
            Constants.SquareBegin, Constants.SquareEnd
        };
        // map open delimeters to their matched closing ones
        private static readonly Map<String, String> DelimMap = new Map<string, string>
        {
            [Constants.ParenBegin] = Constants.ParenEnd,
            [Constants.CurlyBegin] = Constants.CurlyEnd,
            [Constants.SquareBegin] = Constants.SquareEnd
        };

        public static bool IsDelimiter(char c)
        {
            return Delims.Contains(c.ToString());
        }

        public static bool IsOpen(Node c)
        {
            if (c is DelimeterNode delimeter)
            {
                return DelimMap.Keys.Contains((delimeter).Shape);
            }
            return false;
        }

        public static bool IsClose(Node c)
        {
            if (c is DelimeterNode delimeter)
            {
                return DelimMap.Values.Contains(delimeter.Shape);
            }
            return false;
        }

        public static bool IsMatch(Node open, Node close)
        {
            if (!(open is DelimeterNode left) || !(close is DelimeterNode right))
            {
                return false;
            }
            String matched = DelimMap.Get(left.Shape);
            return matched != null && matched.Equals(right.Shape);
        }

        #endregion

        public readonly String Shape;


        public DelimeterNode(String shape, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Shape = shape;
        }
        
        public override Value Interp(Scope s)
        {
            throw new NotSupportedException();
        }

        public override String ToString()
        {
            return Shape;
        }
    }
}
