﻿using System;
using System.Numerics;

namespace TLang.Ast
{
    using Values;

    public class BigIntNode : Node
    {
        private readonly String _content;
        private readonly BigInteger _value;


        public BigIntNode(String content, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this._content = content;

            int sign;
            BigInteger value1;
            if (content.StartsWith("+"))
            {
                sign = 1;
                content = content.Substring(1);
            }
            else if (content.StartsWith("-"))
            {
                sign = -1;
                content = content.Substring(1);
            }
            else
            {
                sign = 1;
            }

            if (content.StartsWith("0b"))
            {
                content = content.Substring(2);
                value1 = new BigInteger(Convert.ToInt64(content, 2));
            }
            else if (content.StartsWith("0x"))
            {
                content = content.Substring(2);
                value1 = BigInteger.Parse(content, System.Globalization.NumberStyles.HexNumber);
            }
            else if (content.StartsWith("0o"))
            {
                content = content.Substring(2);
                value1 = new BigInteger(Convert.ToInt64(content, 8));
            }
            else
            {
                value1 = BigInteger.Parse(content);
            }

            if (sign == -1)
            {
                value1 = -value1;
            }
            this._value = value1;
        }


        public static BigIntNode Parse(String content, String file, int start, int end, int line, int col)
        {
            try
            {
                return new BigIntNode(content, file, start, end, line, col);
            }
            catch (FormatException)
            {
                return null;
            }
        }


        public override Value Interp(Scope s)
        {
            return null;
        }

        public override String ToString()
        {
            return _content;
        }
    }
}
