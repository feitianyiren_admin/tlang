﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TLang.Ast
{
    using Values;

    public abstract class Node
    {
        public readonly String File;
        public readonly int Start;
        public readonly int End;
        public readonly int Line;
        public readonly int Col;

        public Type NodeType
        {
            get { return this.GetType(); }
        }


        protected Node(String file, int start, int end, int line, int col)
        {
            this.File = file;
            this.Start = start;
            this.End = end;
            this.Line = line;
            this.Col = col;
        }


        public abstract Value Interp(Scope s);


        public static Value Interp(Node node, Scope s)
        {
            return node.Interp(s);
        }

        public static List<Value> InterpList(IEnumerable<Node> nodes, Scope s)
        {
            List<Value> values = new List<Value>();
            foreach (Node n in nodes)
            {
                values.Add(n.Interp(s));
            }
            return values;
        }

        public String GetFileLineCol()
        {
            return File + ":" + (Line + 1) + ":" + (Col + 1);
        }


        public static String PrintList(IEnumerable<Node> nodes)
        {
            StringBuilder sb = new StringBuilder();
            bool first = true;
            foreach (Node e in nodes)
            {
                if (!first)
                {
                    sb.Append(" ");
                }
                sb.Append(e);
                first = false;
            }
            return sb.ToString();
        }
    }
}
