﻿using System;

namespace TLang.Ast
{
    using Values;

    public class SetNode : Node
    {
        private readonly Node _pattern;
        private readonly Node _value;


        public SetNode(Node pattern, Node value, String file, int start, int end, int line, int col): base(file, start, end, line, col)
        { 
            this._pattern = pattern;
            this._value = value;
        }
        
        public override Value Interp(Scope s)
        {
            Value value = _value.Interp(s);
            if (_pattern is DotNode dotNode)
            {
                Binder.Set(dotNode.Locate(s), value, s);
            }
            else
            {
                Binder.Set(_pattern, value, s);
            }
            return value;
        }

        public override String ToString()
        {
            return "(" + Constants.SetKeyword + " " + _pattern + " " + _value + ")";
        }
    }
}
