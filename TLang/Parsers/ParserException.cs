﻿using System;

namespace TLang.Parsers
{
    using Ast;

    public class ParserException : Exception
    {
        public ParserException(String message, int line, int col, int start) 
            : base((line + 1) + ":" + (col + 1) + " parsing error " + message)
        {
            this.Line = line;
            this.Col = col;
            this.Start = start;
        }

        public ParserException(String message, Node node) 
            : this(message, node.Line, node.Col, node.Start)
        {
            this.Line = node.Line;
            this.Col = node.Col;
            this.Start = node.Start;
        }

        public int Line { get; }

        public int Col { get; }

        public int Start { get; }

        public override String ToString()
        {
            return Message;
        }
    }
}
