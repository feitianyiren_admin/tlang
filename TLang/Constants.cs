﻿using System;

namespace TLang
{
    public class Constants
    {
        public const string LineComment = "--";

        public const string ParenBegin = "(";
        public const string ParenEnd = ")";

        public const string CurlyBegin = "{";
        public const string CurlyEnd = "}";

        public const string SquareBegin = "[";
        public const string SquareEnd = "]";

        public const string ReturnArrow = "->";

        public const string StringStart = "\"";
        public const string StringEnd = "\"";
        public const string StringEscape = "\\";
        public const char CharStart = '\'';
        public const char CharEnd = '\'';
        public const char CharEscape = '\\';

        // keywords
        public const string SeqKeyword = "seq";
        public const string FunKeyword = "fun";
        public const string IfKeyword = "if";
        public const string CondKeyword = "cond";
        public const string CaseKeyword = "case";
        public const string ForKeyword = "for";
        public const string WhileKeyword = "while";
        public const string DefKeyword = "define";
        public const string SetKeyword = "set!";
        public const string IncKeyword = "inc!";
        public const string DecKeyword = "dec!";
        public const string RecordKeyword = "record";
        public const string DeclareKeyword = "declare";
        public const string AndKeyword = "&&";// and or函数贪婪计算，&& ||短路计算。
        public const string OrKeyword = "||";
        public const string UnionKeyword = "U";

        public const string ElseKeyword = "else";

        public const char ColonChar = ':';
        public const char DotChar = '.';
        public const char PositiveChar = '+';
        public const char NegativeChar = '-';

        public static readonly Char[] IdentChars = new Char[]{
            '~', '!', '@', '#', '$', '%', '^', '&', '*', NegativeChar, '_'
            , '=', PositiveChar, '|', ColonChar, ';', ',', '<', '>', '?', '/'};

        public static readonly char[] AtributeAccessChars = new Char[]
        {
            DotChar,'#'
        };
    }
}
