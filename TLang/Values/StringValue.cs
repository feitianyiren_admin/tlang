﻿namespace TLang.Values
{
    public class StringValue : Value
    {
        public readonly string Value;

        public StringValue(string value) : base(TType.String)
        {
            this.Value = value;
        }

        public override string ToString()
        {
            return $"\"{Value}\"";
        }
    }
}
