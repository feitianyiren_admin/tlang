﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TLang.Values
{
    using Ast;

    public class RecordType : TType
    {
        private readonly Node _definition;
        public readonly Scope Properties;


        public RecordType(String name, Node definition, Scope properties) : base(name)
        {
            this._definition = definition;
            this.Properties = properties.Copy();
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Constants.ParenBegin);
            sb.Append(Constants.RecordKeyword).Append(" ");
            sb.Append(Name == null ? "_" : Name);

            foreach (string field in Properties.KeysLocal)
            {
                sb.Append(" ").Append(Constants.SquareBegin);
                sb.Append(field);

                Dictionary<string, object> m = Properties.LookupAllProps(field);
                foreach (string key in m.Keys)
                {
                    m.TryGetValue(key, out object value);
                    if (value != null)
                    {
                        sb.Append(" :" + key + " " + value);
                    }
                }
                sb.Append(Constants.SquareEnd);
            }

            sb.Append(Constants.ParenEnd);

            return sb.ToString();
        }
    }
}
