﻿using System;
using System.Collections.Generic;

namespace TLang.Values
{
    using Ast;

    public abstract class PrimFun : Value
    {
        public readonly String Name;
        public readonly int Arity;
        
        protected PrimFun(String name, int arity) : base(TType.Fun)
        {
            this.Name = name;
            this.Arity = arity;
        }
        
        // how to apply the primitive to args (must be positional)
        public abstract Value Apply(List<Value> args, Node location);
        
        public override string ToString()
        {
            return Name;
        }
    }
}
