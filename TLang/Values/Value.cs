﻿namespace TLang.Values
{
    public abstract class Value
    {
        public TType Type { get; protected set; }

        protected Value(TType type)
        {
            this.Type = type;
        }
    }
}
