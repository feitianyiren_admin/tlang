﻿using System.Collections.Generic;
using TLang.Ast;

namespace TLang.Values.Primitives
{
    public class Str : PrimFun
    {
        public Str() : base("str", 1)
        {

        }

        public override Value Apply(List<Value> args, Node location)
        {
            Value value = args[0];
            if (value is StringValue)
            {
                return value;
            }
            return new StringValue(value.ToString());
        }
    }
}
