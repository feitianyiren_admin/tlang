namespace TLang.Values.Primitives
{
    using Ast;
    using System.Collections.Generic;

    /// <summary>
    /// or����̰�����㣬||��·����
    /// </summary>
    public class Or : PrimFun
    {
        public Or() : base("or", 2)
        {
            
        }

        public override Value Apply(List<Value> args, Node location)
        {

            Value v1 = args[0];
            Value v2 = args[1];
            if (v1 is BoolValue && v2 is BoolValue)
            {
                return BoolValue.Of(((BoolValue)v1).Value || ((BoolValue)v2).Value);
            }

            throw Util.GeneralError(location, "incorrect argument types for and: " + v1 + ", " + v2);
        }
    }
}