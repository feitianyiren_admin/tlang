namespace TLang.Values.Primitives
{
    using Ast;
    using System.Collections.Generic;

    public class Not : PrimFun
    {
        public Not() : base("not", 1)
        {
            
        }

        public override Value Apply(List<Value> args, Node location)
        {

            Value v1 = args[0];
            if (v1 is BoolValue bv)
            {
                return BoolValue.Of(!bv.Value);
            }
            throw Util.GeneralError(location, "incorrect argument type for not: " + v1);
        }
    }
}