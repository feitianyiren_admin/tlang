﻿using System.Collections.Generic;
using TLang.Ast;

namespace TLang.Values.Primitives
{
    public class Mod : PrimFun
    {
        public Mod() : base("%", 2)
        {

        }

        public override Value Apply(List<Value> args, Node location)
        {
            Value v1 = args[0];
            Value v2 = args[1];
            if (v1 is IntValue && v2 is IntValue)
            {
                return new IntValue(((IntValue)v1).Value % ((IntValue)v2).Value);
            }

            throw Util.GeneralError(location, "incorrect argument types for %: " + v1 + ", " + v2);
        }
    }
}