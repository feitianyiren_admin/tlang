﻿using System.Collections.Generic;
using TLang.Ast;

namespace TLang.Values.Primitives
{
    public class Len : PrimFun
    {
        public Len() : base("len", 1)
        {

        }

        public override Value Apply(List<Value> args, Node location)
        {
            Value v1 = args[0];
            if (v1 is VectorValue vector)
            {
                return new IntValue(vector.Count);
            }
            if (v1 is RecordType record)
            {
                return new IntValue(record.Properties.KeysLocal.Count);
            }
            if (v1 is StringValue str)
            {
                return new IntValue(str.Value.Length);
            }

            throw Util.GeneralError(location, "incorrect argument types for +: " + v1);
        }
    }
}
