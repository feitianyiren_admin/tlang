﻿namespace TLang.Values
{
    public class IntValue : Value
    {
        public readonly int Value;

        public IntValue(int value) : base(TType.Int)
        {
            this.Value = value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
