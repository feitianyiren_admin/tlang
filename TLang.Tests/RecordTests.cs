﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TLang.Values;

namespace TLang.Tests
{
    [TestClass]
    public class RecordTests
    {
        [TestMethod]
        public void DefineLiteralRecord()
        {
            string text = @"
(define r {:x 1 :y 2})
r";
            RecordType value = (RecordType)Interpreter.InterpText(text);
            Map<string, object> xProperties = value.Properties.LookupAllProps("x");
        }
    }
}
