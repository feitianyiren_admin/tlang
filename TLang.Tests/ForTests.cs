﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TLang.Values;

namespace TLang.Tests
{
    [TestClass]
    public class ForTests
    {
        [TestMethod]
        public void ForTest1()
        {
            string text = @"
(define sum 0)
(for (define i 0) (< i 10) (inc! i)
    (set! sum (+ sum i)))";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(Enumerable.Range(0, 10).Sum(), value.Value);
        }

        [TestMethod]
        public void ForTest2()
        {
            string text = @"
(define sum 0)
(define i 1.1)
(for (define i 0) (< i 10) (inc! i)
    (set! sum (+ sum i)))
[i sum]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(1.1, ((FloatValue)value[0]).Value);
            Assert.AreEqual(Enumerable.Range(0, 10).Sum(), ((IntValue)value[1]).Value);
        }

        [TestMethod]
        public void ForTest3()
        {
            string text = @"
(define sum 0)
(define i 0)
(while (< i 10)
    (set! sum (+ sum i))
    (inc! i))
[i sum]"; 
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(10, ((IntValue)value[0]).Value);
            Assert.AreEqual(Enumerable.Range(0, 10).Sum(), ((IntValue)value[1]).Value);
        }
    }
}
