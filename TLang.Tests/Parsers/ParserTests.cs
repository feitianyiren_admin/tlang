﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TLang.Parsers;
using TLang.Ast;

namespace TLang.Tests.Parsers
{
    [TestClass]
    public class ParserTests
    {
        [TestMethod]
        [ExpectedException(typeof(GeneralError))]
        public void Test1()
        {
            string text = @"if";
            Node node = Parser.Parse(text, null);
            Assert.IsNotNull(node);

            var value = node.Interp(Scope.BuildInitScope());
        }

        [TestMethod]
        [ExpectedException(typeof(GeneralError))]
        public void Test2()
        {
            string text = @"(a)";
            Node node = Parser.Parse(text, null);
            Assert.IsNotNull(node);

            var value = node.Interp(Scope.BuildInitScope());
        }

        [TestMethod]
        [ExpectedException(typeof(GeneralError))]
        public void Test3()
        {
            string text = @"(a b)";
            Node node = Parser.Parse(text, null);
            Assert.IsNotNull(node);

            var value = node.Interp(Scope.BuildInitScope());
        }

        [TestMethod]
        [ExpectedException(typeof(ParserException))]
        public void Test4()
        {
            string text = @"(";
            Node node = Parser.Parse(text, null);
            Assert.IsNull(node);
        }

        [TestMethod]
        [ExpectedException(typeof(ParserException))]
        public void Test5()
        {
            string text = @")";
            Node node = Parser.Parse(text, null);
            Assert.IsNull(node);
        }

        [TestMethod]
        [ExpectedException(typeof(ParserException))]
        public void Test6()
        {
            string text = @"(assda";
            Node node = Parser.Parse(text, null);
            Assert.IsNull(node);
        }

        [TestMethod]
        [ExpectedException(typeof(ParserException))]
        public void Test7()
        {
            string text = @"sdfsd)";
            Node node = Parser.Parse(text, null);
            Assert.IsNull(node);
        }

        [TestMethod]
        [ExpectedException(typeof(ParserException))]
        public void Test8()
        {
            string text = @"(define";
            Node node = Parser.Parse(text, null);
            Assert.IsNull(node);
        }

        [TestMethod]
        [ExpectedException(typeof(GeneralError))]
        public void Test9()
        {
            string text = @"
(f foo.x.y ""hello world"" (z 12.4))";
            Node node = Parser.Parse(text, null);
            Assert.IsNotNull(node);
            var value = node.Interp(Scope.BuildInitScope());
        }
    }
}
