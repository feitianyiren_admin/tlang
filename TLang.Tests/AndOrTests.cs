﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TLang.Values;

namespace TLang.Tests
{
    // and or贪婪计算，&& ||短路计算
    [TestClass]
    public class AndOrTests
    {
        [TestMethod]
        public void AndFunTest1()
        {
            string text = @"
(define x 1)
(and true (seq (set! x 2) (= 1 2)))
x";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void AndShortcutTest1()
        {
            string text = @"
(define x 1)
(&& true (seq (set! x 2) (= 1 2)))
x";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void AndShortcutTest2()
        {
            string text = @"
(define x 1)
(&& false (seq (set! x 2) (= 1 2)))
x";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(1, value.Value);
        }
        
        [TestMethod]
        public void OrFunTest1()
        {
            string text = @"
(define x 1)
(or false (seq (set! x 2) (= 1 2)))
x";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void OrShortcutTest1()
        {
            string text = @"
(define x 1)
(|| false (seq (set! x 2) (= 1 2)))
x";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void OrShortcutTest2()
        {
            string text = @"
(define x 1)
(|| true (seq (set! x 2) (= 1 2)))
x";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(1, value.Value);
        }
    }
}
