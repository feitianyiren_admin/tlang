﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TLang.Values;

namespace TLang.Tests
{
    [TestClass]
    public class TypeTests
    {
        [TestMethod]
        public void IsTest()
        {
            string text = @"
(define a 1)

[(is? Int a)
(is? Int 1)
(is? Float 1.0)
(is? String ""aaa"")
(is? Bool true)
(is? Vector [])
(is? Any 1)
(is? Any 1.0)
(is? Any ""aaa"")
(is? Any true)
(is? Any [])
(is? String (str 1))
(= ""1"" (str 1))]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            for (int i = 0; i < value.Count; i++)
            {
                Assert.AreEqual(true, ((BoolValue)value[i]).Value);
            }
        }
    }
}
