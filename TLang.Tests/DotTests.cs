﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TLang.Parsers;
using TLang.Values;

namespace TLang.Tests
{
    [TestClass]
    public class DotTests
    {
        [TestMethod]
        public void DotTest1()
        {
            string text = @"
(define a [1 2 3])
a.1";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void DotTest2()
        {
            string text = @"
(define a [1 [11 22 33] 3])
(define x a.1.1)
(set! a.1.1 23)
[
    (= x 22)
    (= a.1.2 33)
    (= a.(a.0).1 23)
    (= a.(seq (set! x 1) x).2 33)
    (= a.(seq (set! x 1)).2 33)
    (= a.(set! x 1).2 33)
    (= a.((fun (x y) (+ x y)) 0 1).2 33)
    (= a.((fun (y) (set! x y)) 0) 1)
    (= x 0)
    (= a.((fun () (set! x 1))).2 33)
    (= x 1)
]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            for (int i = 0; i < value.Count; i++)
            {

                Assert.IsTrue(((BoolValue)value[i]).Value);
            }
        }

        [TestMethod]
        public void DotTest3()
        {
            string text = @"
(define a [1 2 3 (fun (x) (+ x 1))])
(define b {:x 1 :f (fun (x) (+ x 1))})
[a.1 (a.3 1) (b.f 1)]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, ((IntValue)value[0]).Value);
            Assert.AreEqual(2, ((IntValue)value[1]).Value);
            Assert.AreEqual(2, ((IntValue)value[2]).Value);
        }

        [TestMethod]
        [ExpectedException(typeof(ParserException))]
        public void DotTest4()
        {
            string text = @"
(define a.b 1)";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        [ExpectedException(typeof(ParserException))]
        public void DotTest5()
        {
            string text = @"
(define (f.a x) (+ x 1))";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void DotTest6()
        {
            string text = @"
(define a [1 2 3 4 5 (+ 1 1)])
(define f (fun (x) x))
[a.(+ 1 1) a.(f 2)]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, ((IntValue)value[0]).Value);
            Assert.AreEqual(3, ((IntValue)value[1]).Value);
        }
    }
}
